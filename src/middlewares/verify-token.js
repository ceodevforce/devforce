import jwt from 'jsonwebtoken';

export default (req, res, next) => {
    const token = req.get('x-auth-token');
    try {
        jwt.verify(token, 'secret');
        next();
    } catch (error) {
        res.status(401).send({ error: 'Token is not valid' });
    }
}