/**
 * Requires:
 */
import * as dotenv from 'dotenv';
import express from 'express';
import cors from 'cors';
import helmet from 'helmet';
import { itemsRouter } from "./items/items.router";

dotenv.config();

/**
 * Variables:
 */


if (!process.env.PORT) {
  process.exit(1);
}

const PORT: number = parseInt(process.env.PORT as string, 10);

const app = express();

/**
 * Config:
 */

app.use(helmet());
app.use(cors());
app.use(express.json());
app.use("/api/items", itemsRouter);

/**
 * Setup:
 */
app.listen(PORT, () => {
  console.log(`Server listening on port ${PORT}`);
});