import express, { Request, Response } from "express";
import * as sqlite from "sqlite";

const router = express.Router();
const verifyToken = require("../../middleware/verify-token");

router.get("/", async (req: Request, res: Response) => {
  const db = await new sqlite.Database('./db.sqlite');
    db.serialize(() => {
      db.all(`SELECT bookings.*, catalog_items.name AS catalog_item_name, catalog_items.description AS catalog_item_description 
      FROM bookings 
      INNER JOIN catalog_items ON catalog_items.id = bookings.catalog_item_id`, [], (err, rows) => {
        if (err) {
          res.status(500).json({
            message: "Error fetching bookings",
            error: err
          });
        } else {
          res.json(rows);
        }
});