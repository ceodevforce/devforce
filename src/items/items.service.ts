// Data models
import { BaseItem, Item } from "./item.interface";
import { Items } from "./items.interface";


/**
 * In-memory database of items.
 */

let items: Items = {
    1: {
        id: 1,
        name: "French Toast",
        price: 4.50,
        description: "Freshly made french toast with butter and maple syrup",
        imageUrl: "https://images.unsplash.com/photo-1484723091739-30a097e8f929?ixlib=rb-1.2.1&ixid=MnwxMjA3fDB8MHxzZWFyY2h8MTN8fGZvb2R8ZW58MHx8MHx8&auto=format&fit=crop&w=500&q=60",
        quantity: 4
    },
    2: {
        id: 2,
        name: "Burger Melt",
        price: 5.50,
        description: "Freshly made burger melt with butter and maple syrup",
        imageUrl: "https://images.unsplash.com/photo-1565299507177-b0ac66763828?ixlib=rb-1.2.1&ixid=MnwxMjA3fDB8MHxzZWFyY2h8MzN8fGZvb2R8ZW58MHx8MHx8&auto=format&fit=crop&w=500&q=60",
        quantity: 4
    },
    3: {
        id: 3,
        name: "Pasta Sweden",
        price: 6.50,
        description: "Freshly made pasta with butter and maple syrup",
        imageUrl: "https://images.unsplash.com/photo-1600803907087-f56d462fd26b?ixlib=rb-1.2.1&ixid=MnwxMjA3fDB8MHxzZWFyY2h8NjJ8fGZvb2R8ZW58MHx8MHx8&auto=format&fit=crop&w=500&q=60",
        quantity: 4
    },
    4: {
        id: 4,
        name: "Milkshake Chocolate",
        price: 3.50,
        description: "Freshly made milkshake with butter and maple syrup",
        imageUrl: "https://images.unsplash.com/photo-1577805947697-89e18249d767?ixlib=rb-1.2.1&ixid=MnwxMjA3fDB8MHxzZWFyY2h8MTI1fHxmb29kfGVufDB8fDB8fA%3D%3D&auto=format&fit=crop&w=500&q=60",
        quantity: 1
    },
}


/**
 * Items service.
 */
export const findAll = async (): Promise<Item[]> => {
    return Object.values(items);
}

export const findOne = async (id: number): Promise<Item> => {
    return items[id];
}

export const create = async (item: BaseItem): Promise<Item> => {
    const id = Object.keys(items).length + 1;
    items[id] = {
        id,
        ...item
    }
    return items[id];
}

export const update = async (id: number, itemUpdate: BaseItem): Promise<Item | null> => {
    const item = await findOne(id);

    if (!item) {
        return null;
    }
    items[id] = {
        id,
        ...itemUpdate
    }
    return items[id];
}

export const remove = async (id: number): Promise<void | null> => {
    const item = await findOne(id);
    if (!item) {
        return null;
    }
    delete items[id];
}